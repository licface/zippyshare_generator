#!/usr/bin/env python2
import os
import sys
import traceback
# sys.excepthook = traceback.format_exc
import argparse
import clipboard
from configset import configset
from pydebugger.debug import debug
from make_colors import make_colors
import requests
from bs4 import BeautifulSoup as bs
from parserheader import parserheader
import re
from pywget import wget
from pause import pause
try:
    from . import js_exe
except:
    import js_exe

if sys.version_info.major == 3:
    from rich.console import Console
    from fake_headers import Headers
    # print("__name__:", __name__)
    if __name__ == '__main__' or __name__ == '__console__':
        from downloader import download
    else:
        from .downloader import download

    import urllib.request, urllib.parse, urllib.error
else:
    if __name__ == '__main__':
        from downloader2 import download
    else:
        from . downloader2 import download
    import urllib as urllibx
    import urlparse
    class urllib:
        def request(self):
            pass
        def parse(self):
            pass            
        def error(self):
            pass
    urllib.request = urllibx
    urllib.parse = urlparse
    urllib.error = urllibx


class zippyshare(object):
    def __init__(self, url = None, download_path = os.getcwd(), altname = None):
        super(zippyshare, self)
        self.debug = False
        
        if not os.path.isfile(os.path.join(os.path.dirname(__file__), 'zippyshare.ini')):
            configname = os.path.join(os.path.dirname(__file__), 'zippyshare.ini')
        else:
            configname = os.path.join(os.path.dirname(__file__), 'zippyshare.ini')
        self.config = configset(configname)        
        if url:
            self.url = url
            url_download, name = self.generate(self.url)
            if name:
                altname = name
            self.download(url_download, download_path, altname)
        
        
    def generate(self, url):
        # os.environ.update({'DEBUG':'1'})
        debug(url = url)
        # purl = urllib.parse.urlparse(url)
        # debug(purl = purl)
        # debug(purl_path = purl.path)

        code = re.findall('http.*?/[a-z]/(.*?)/', url)
        debug(code = code)
        
        try:
            www = re.findall('https:/.(.*?).zippyshare', url)[0]
            debug(www = www, debug = self.debug)
            print(make_colors("URL", 'lw' 'bl') + " : " + make_colors(url, 'b', 'lg'))
        except:
            if len(url) > 80:
                print(make_colors("URL", 'lw' 'bl') + " : " + make_colors(url[:51], 'lw', 'm'))
            else:
                print(make_colors("URL", 'lw' 'bl') + " : " + make_colors(url, 'lw', 'm'))
            print(make_colors("Invalid Link !", 'lw', 'lr', ['blink']))
        
        header = {}
        while 1:
            try:
                a = requests.get(url, headers = header)
                break
            except:
                traceback.format_exc()
        b = bs(a.content, 'lxml')
        scripts = b.find('script', text = re.compile("document\.getElementById('dlbutton')\.href"))
        debug(scripts = scripts)

        name = ''
        name = b.find('table', {'class':'folderlogo'}).find('div', {'class':'center'}).find('font', text=re.compile("\.mp4"))
        
        if name:
            name = name.text
        debug(name = name)
        #
        try:
            js_script = b.find("div", {'class': 'center',}).find_all("script")[1]
        except:
            js_script = b.find("div", {'class': 'right',}).find_all("script")[0]
        
        debug(js_script = js_script)
        js_content = ""
        # js_content = re.findall('\.href.=."/(.*?)";', str(js_script))
        js_content = re.findall('"/d/' + code[0] + '/" \+ (\(.*?\))', js_script.text)
        debug(js_content1 = js_content)
        nfile = re.findall('"/d/' + code[0] + '/" \+ \(.*?\) \+ \"(.*?)\";', js_script.text)
        debug(nfile = nfile)
        # pause()
        # data_a = re.findall("a = (\d+);", str(js_script))
        # debug(data_a = data_a)
        # if not data_a:
        #     return False, False
        
        # data_b = re.findall("b = (\d+);", str(js_script))
        # debug(data_b = data_b)
        # if not data_b:
        #     return False, False
        
        # js_content = 'var x = "/' + js_content[0] + '"'
        js_content = 'var x = ' + js_content[0] 
        debug(js_content = js_content)
        # js_content = re.sub("a +", "Math.floor(" + data_a[0] + "/3)" + " ", js_content)
        # debug(js_content = js_content)
        # # sys.exit()
        # js_content = re.sub("%b", "%"+data_b[0], js_content)
        # debug(js_content = js_content)
        js_content = js_exe.generator(js_content, "x")
        debug(js_content = js_content)
        
        # url_download = 'https://' + str(www) + ".zippyshare.com" + str(js_content)
        url_download = 'https://' + str(www) + ".zippyshare.com" + "/d/" + code[0] + "/" + str(js_content) + nfile[0]
        debug(url_download = url_download, debug = self.debug)
        # pause()
        
        return url_download, name
    
    def download(self, url, download_path = os.getcwd(), altname = None, prompt = False):
        debug(url = url)
        if not url:
            print(make_colors("GENERATING FAILED !", 'lw', 'r'))
            sys.exit()
        print(make_colors("SAVEAS (altname):", 'lw', 'bl') + " " + make_colors(altname))
        try:
            import idm
            dm = idm.IDMan()
            dm.download(url, download_path, altname, confirm= prompt)
        except:
            try:
                if altname:
                    print("altname =", altname)
                    print('url     =', url)
                    download(url, download_path, os.path.splitext(altname)[0], ext = os.path.splitext(url)[1][1:])
                else:
                    download(url, download_path, altname, ext = os.path.splitext(url)[1][1:])
            except:
                print("ERROR:", traceback.format_exc())
                sys.exit()
                if os.getenv('debug'):
                    traceback.format_exc()
                if altname:
                    download_path = os.path.join(download_path, altname)
                wget.download(url, download_path)
        
    def parseheader(self, header_text = None):
        default  = """
    HTTP/1.1 200 Connection established
Server: nginx
Date: Thu, 12 Sep 2019 10:03:26 GMT
Content-Type: text/html;charset=UTF-8
Transfer-Encoding: chunked
Connection: keep-alive
Set-Cookie: JSESSIONID=8FFFF1380195C68BA0E0C2C960AD8B32; Path=/; HttpOnly
Set-Cookie: zippop=1; Domain=.zippyshare.com; Expires=Thu, 12-Sep-2019 22:03:26 GMT; Path=/
Content-Language: en
Expires: Thu, 12 Sep 2019 10:03:25 GMT
Cache-Control: no-cache
Strict-Transport-Security: max-age=31536000; includeSubDomains; preload
Content-Encoding: gzip
    """
        if not header_text: 
            header_text = self.config.read_config('header', 'text', value= default)
            debug(header_text = header_text, debug = self.debug)
        p = parserheader()
        header = p.parserHeader(header_text)
        debug(header = header, debug = self.debug)

    def usage(self):
        parser = argparse.ArgumentParser(formatter_class= argparse.RawTextHelpFormatter)
        parser.add_argument('URL', action = 'store', help = 'Zippyshare url, example: "https://www48.zippyshare.com/v/pedPCo05/file.html", type "c" for get url from clipboard')
        parser.add_argument('-p', '--download-path', action = 'store', help = 'Download path to save file')
        parser.add_argument('-n', '--name', action = 'store', help = 'Alternative Save as name')
        parser.add_argument('-P', '--prompt', action = 'store_true', help = 'Prompt Before download')
        parser.add_argument('-d', '--debug', action = 'store_true', help = 'Debugger process')
        parser.add_argument('-c', '--clipboard', action = 'store_true', help = 'Copy generated link to clipboard')
        if len(sys.argv) == 1:
            parser.print_help()
        else:
            args = parser.parse_args()
            debug(debugger = args.debug)            
            if self.config.read_config('debug', 'debug', value= False):
                self.debug = eval(self.config.read_config('debug', 'debug', value= False))
                debug(self_debug = self.debug)
            self.debug = args.debug
            debug(self_debug = self.debug)            
            if args.URL == 'c':
                args.URL = clipboard.paste()
            url_download, name = self.generate(args.URL)
            if name and not args.name:
                args.name = name
            if args.download_path:
                self.download(url_download, args.download_path, args.name, args.prompt)
            else:
                print(make_colors("GENERATED:", 'w', 'r') + " " + make_colors(url_download, 'b', 'ly', attrs= ['blink']))
                if args.clipboard:
                    clipboard.copy(url_download)                

if __name__ == '__main__':
    c = zippyshare()
    # url, name = c.generate("https://www60.zippyshare.com/v/Vz18IOGP/file.html")
    # print("URL :", url)
    # print("NAME:", name)
    c.usage()
